$(function(){

   var moBut = $(".mobMenuBlock");

   var mobMenu = $(".headMenuList");

   moBut.on("click", function(){
        if(mobMenu.hasClass("mobOpened")){
            mobMenu.removeClass("mobOpened");
            mobMenu.slideUp();
            mobMenu.parent().parent().find(".mobText").html("Open Mobile Menu");
        }
        else{
            mobMenu.addClass("mobOpened");
            mobMenu.slideDown();
            mobMenu.parent().parent().find(".mobText").html("Close Mobile Menu");
        }
   });


});