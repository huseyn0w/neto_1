var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync');
var flatten = require('gulp-flatten');
var rigger = require('gulp-rigger');

gulp.task('rigger', function () {
	gulp.src('app/template/*.html')
		.pipe(rigger())
		.pipe(gulp.dest('app/'))
		.pipe(browserSync.reload({stream: true}))
});
gulp.task('sass',function(){
    return gulp.src('app/sass/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.reload({stream: true}))
});
gulp.task('browser-sync',function(){
    browserSync({
       server: {
           baseDir: 'app'
       },
       notify:false
    });
});
gulp.task('watch',['sass','rigger','browser-sync'],function(){
    gulp.watch('app/sass/**/**/**/*.scss',['sass']);   
	gulp.watch('app/template/**/**/*.html',['rigger']);	
    gulp.watch('app/*.html', browserSync.reload);
    gulp.watch('app/js/**/*.js', browserSync.reload);
});
gulp.task('default', ['watch','browser-sync']);